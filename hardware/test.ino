#include <Firebase_ESP_Client.h>
#include "Wire.h"
#include <WiFi.h>
#include "DHT.h"
#include "addons/TokenHelper.h"
#include "addons/RTDBHelper.h"

#define DHTPIN 4
#define DHTTYPE DHT11
#define MQ05 34
#define KY26 2
#define WIFI_SSID "manh"
#define WIFI_PASSWORD "12345678"
#define API_KEY "AIzaSyB5G2mgdKf1-8gUJJkc0DvZg197hjPevuQ"
#define USER_EMAIL "home1@firebase.com"
#define USER_PASSWORD "home123456"
#define DATABASE_URL "https://fire-base-sensor-default-rtdb.asia-southeast1.firebasedatabase.app"
DHT dht(DHTPIN, DHTTYPE);

FirebaseData fbdo;
FirebaseAuth auth;
FirebaseConfig config;

String uid;

String databasePath;
String tempPath;
String humPath;
String gasPath;
String sirenPath;
String tapPath;
String flamePath;

float temperature;
float humidity;
int gasValue;
float gas;
int flame;
int relayState;
const float thresholdGas = 0.5;
const float thresholdTemp = 50;

unsigned long sendDataPrevMillis = 0;
unsigned long timerDelay = 2000;

unsigned long lastDebounceTime = 0;  // Variable to store the last time the button was pressed
unsigned long debounceDelay = 50;

const int output1 = 15;
const int output2 = 14;

void initWiFi() {
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Connecting to WiFi ..");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
  }
  Serial.println(WiFi.localIP());
  Serial.println();
}

void sendFloat(String path, float value){
  if (Firebase.RTDB.setFloat(&fbdo, path.c_str(), value)){
    Serial.print("Writing value: ");
    Serial.print (value);
    Serial.print(" on the following path: ");
    Serial.println(path);
    Serial.println("PASSED");
    Serial.println("PATH: " + fbdo.dataPath());
    Serial.println("TYPE: " + fbdo.dataType());
  }
  else {
    Serial.println("FAILED");
    Serial.println("REASON: " + fbdo.errorReason());
  }
}

void getState(String path, int gpio){
  if (Firebase.RTDB.getInt(&fbdo, path.c_str())){
    relayState = fbdo.intData();
    Serial.println("PATH: " + fbdo.dataPath());
    Serial.print("relay state is:");
    Serial.print(relayState);
    if(relayState == 1){
      digitalWrite(gpio, HIGH);
    }
    else if(relayState == 0){
      digitalWrite(gpio, LOW);
    }
  }
  else {
    Serial.println("FAILED");
    Serial.println("REASON: " + fbdo.errorReason());
  }
}

void setup(){
  Serial.begin(115200);
  pinMode(output1, OUTPUT);
  pinMode(output2, OUTPUT);
  pinMode(KY26, INPUT);
  pinMode(MQ05, INPUT);
  dht.begin();

  // Initialize BME280 sensor
  initWiFi();

  // Assign the api key (required)
  config.api_key = API_KEY;

  // Assign the user sign in credentials
  auth.user.email = USER_EMAIL;
  auth.user.password = USER_PASSWORD;

  // Assign the RTDB URL (required)
  config.database_url = DATABASE_URL;

  Firebase.reconnectWiFi(true);
  fbdo.setResponseSize(4096);

  // Assign the callback function for the long running token generation task */
  config.token_status_callback = tokenStatusCallback; //see addons/TokenHelper.h

  // Assign the maximum retry of token generation
  config.max_token_generation_retry = 5;

  // Initialize the library with the Firebase authen and config
  Firebase.begin(&config, &auth);

  // Getting the user UID might take a few seconds
  Serial.println("Getting User UID");
  while ((auth.token.uid) == "") {
    Serial.print('.');
    delay(1000);
  }
  // Print user UID
  uid = auth.token.uid.c_str();
  Serial.print("User UID: ");
  Serial.println(uid);

  // Update database path
  databasePath = "/UsersData/" + uid;

  // Update database path for sensor readings
  tempPath = databasePath + "/temperature"; // --> UsersData/<user_uid>/temperature
  humPath = databasePath + "/humidity"; // --> UsersData/<user_uid>/re
  gasPath = databasePath + "/gas";
  tapPath = databasePath + "/relays/12";
  sirenPath = databasePath + "/relays/14";
  flamePath = databasePath + "/relays/2";
}

void loop(){
  if (Firebase.ready() && (millis() - sendDataPrevMillis > timerDelay || sendDataPrevMillis == 0)){
    sendDataPrevMillis = millis();

    // Get latest sensor readings
    temperature = dht.readTemperature();
    humidity = dht.readHumidity();
    gasValue = analogRead(MQ05);
    gas = static_cast<float>(gasValue) / 1800.0;
    flame = digitalRead(KY26);
    Serial.println(temperature);
    Serial.println(humidity);
    Serial.println(gasValue);
    Serial.println(flame);
    if (flame == 1) {
      Serial.println("Flame detected!");
    } else {
      Serial.println("No flame detected.");
    }

    // Send readings to database:
    sendFloat(tempPath, temperature);
    sendFloat(humPath, humidity);
    sendFloat(gasPath, gas);
  }
  if(temperature>=thresholdTemp || gas >= thresholdGas || flame == 1){
    sendFloat(tapPath,1);
    sendFloat(sirenPath,1);
    sendFloat(flamePath,1);
  }
  delay(1000);
  getState(tapPath,output1);
  getState(sirenPath,output2);
  delay(1000);
}