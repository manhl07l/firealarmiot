messaging.requestPermission()
    .then(() => {
    console.log('Notification permission granted.');
    return messaging.getToken();
  })
  .then((token) => {
    console.log('FCM Token:', token);
  })
  .catch((error) => {
    console.error('Unable to get permission to notify.', error);
  });

  messaging.onMessage((payload) => {
  console.log('Message received:', payload);
});

var uid = 'QjOXgltiRQd73bXLVb2Y9KmH75r1';
var dbPathTemp = 'UsersData/' + uid.toString() + '/temperature';
var dbPathHum = 'UsersData/' + uid.toString() + '/humidity';
var dbPathGas = 'UsersData/' + uid.toString() + '/gas';
var dbPathFlame = 'UsersData/' + uid.toString() + '/relays/2';
var dbRefTemp = firebase.database().ref().child(dbPathTemp);
var dbRefHum = firebase.database().ref().child(dbPathHum);
var dbRefGas = firebase.database().ref().child(dbPathGas);
var dbRefFlame = firebase.database().ref().child(dbPathFlame);
let notificationSent = false;

function sendPushNotification(notificationTitle,notificationOptions) {
  if (Notification.permission === 'granted') {
    const notification = new Notification(notificationTitle, notificationOptions);
  } else if (Notification.permission !== 'denied') {
    Notification.requestPermission().then((permission) => {
      if (permission === 'granted') {
        const notification = new Notification(notificationTitle, notificationOptions);
      }
    });
  }
}

const notificationTitle = 'WARNING';
const notificationOptions = {
  body: 'DETECTIVE FIRE',
};

dbRefTemp.on('value', (snapshot) => {
  const temperature = snapshot.val();
  console.log('Temperature:', temperature);
  // Check if the temperature is greater than 50
    // Send a push notification
    if (temperature >= 50 && !notificationSent) {
      // Send a push notification
      sendPushNotification(notificationTitle,notificationOptions);
  
      // Set the flag to true to indicate that the notification has been sent
      notificationSent = true;
    } else if (temperature < 50) {
      // Reset the flag when the temperature falls below or equal to 50
      notificationSent = false;
    }
});

dbRefGas.on('value', (snapshot) => {
  const gas = snapshot.val();
  console.log('Gas:', gas);
  // Check if the temperature is greater than 50
    // Send a push notification
    if (gas >= 0.5 && !notificationSent) {
      // Send a push notification
      sendPushNotification(notificationTitle,notificationOptions);
  
      // Set the flag to true to indicate that the notification has been sent
      notificationSent = true;
    } else if (gas < 0.5) {
      // Reset the flag when the temperature falls below or equal to 50
      notificationSent = false;
    }
});

dbRefFlame.on('value', (snapshot) => {
  const flame = snapshot.val();
  console.log('FLame:', flame);
  // Check if the temperature is greater than 50
    // Send a push notification
    if (flame == 1 && !notificationSent) {
      // Send a push notification
      sendPushNotification(notificationTitle,notificationOptions);
  
      // Set the flag to true to indicate that the notification has been sent
      notificationSent = true;
    } else {
      // Reset the flag when the temperature falls below or equal to 50
      notificationSent = false;
    }
});