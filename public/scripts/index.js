const loginElement = document.querySelector('#login-form');
const contentElement = document.querySelector("#content-sign-in");
const userDetailsElement = document.querySelector('#user-details');
const authBarElement = document.querySelector("#authentication-bar");

// Elements for sensor readings
const tempElement = document.getElementById("temp");
const humElement = document.getElementById("hum");
const gasElement = document.getElementById("gas");
const stateElement1 = document.getElementById("state1");
const btn1On = document.getElementById('btn1On');
const btn1Off = document.getElementById('btn1Off');
const stateElement2 = document.getElementById("state2");
const btn2On = document.getElementById('btn2On');
const btn2Off = document.getElementById('btn2Off');

// MANAGE LOGIN/LOGOUT UI
const setupUI = (user) => {
  if (user) {
    //toggle UI elements
    loginElement.style.display = 'none';
    contentElement.style.display = 'block';
    authBarElement.style.display ='block';
    userDetailsElement.style.display ='block';
    userDetailsElement.innerHTML = user.email;

    // get user UID to get data from database
    var uid = user.uid;
    console.log(uid);

    // Database paths (with user UID)
    var dbPathTemp = 'UsersData/' + uid.toString() + '/temperature';
    var dbPathHum = 'UsersData/' + uid.toString() + '/humidity';
    var dbPathGas = 'UsersData/' + uid.toString() + '/gas';
    var dbPathOutput1 = 'UsersData/' + uid.toString() + '/relays/12';
    var dbPathOutput2 = 'UsersData/' + uid.toString() + '/relays/14';

    // Database references
    var dbRefTemp = firebase.database().ref().child(dbPathTemp);
    var dbRefHum = firebase.database().ref().child(dbPathHum);
    var dbRefGas = firebase.database().ref().child(dbPathGas);
    var dbRefOutput1 = firebase.database().ref().child(dbPathOutput1);
    var dbRefOutput2 = firebase.database().ref().child(dbPathOutput2);


    // Update page with new readings
    dbRefTemp.on('value', snap => {
      tempElement.innerText = snap.val().toFixed(2);
    });

    dbRefHum.on('value', snap => {
      humElement.innerText = snap.val().toFixed(2);
    });

    dbRefGas.on('value', snap => {
      gasElement.innerText = snap.val().toFixed(2);
    });

    dbRefOutput1.on('value', snap => {
      if(snap.val()==1) {
          stateElement1.innerText="ON";
      }
      else{
          stateElement1.innerText="OFF";
      }
    });

    btn1On.onclick = () =>{
      dbRefOutput1.set(1);
    }
    btn1Off.onclick = () =>{
      dbRefOutput1.set(0);
    }

    dbRefOutput2.on('value', snap => {
      if(snap.val()==1) {
          stateElement2.innerText="ON";
      }
      else{
          stateElement2.innerText="OFF";
      }
    });

    btn2On.onclick = () =>{
      dbRefOutput2.set(1);
    }
    btn2Off.onclick = () =>{
      dbRefOutput2.set(0);
    }

  // if user is logged out
  } else{
    // toggle UI elements
    loginElement.style.display = 'block';
    authBarElement.style.display ='none';
    userDetailsElement.style.display ='none';
    contentElement.style.display = 'none';
  }
}