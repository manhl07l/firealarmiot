// firebase-messaging-sw.js

importScripts('https://www.gstatic.com/firebasejs/8.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.8.1/firebase-messaging.js');

// Access Firebase configuration from the main HTML file
const firebaseConfig = {
    apiKey: "AIzaSyB5G2mgdKf1-8gUJJkc0DvZg197hjPevuQ",
    authDomain: "fire-base-sensor.firebaseapp.com",
    databaseURL: "https://fire-base-sensor-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "fire-base-sensor",
    storageBucket: "fire-base-sensor.appspot.com",
    messagingSenderId: "5807118018",
    appId: "1:5807118018:web:8f4bc2b8f5d5f56a3ecd5b"
  };

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler((payload) => {
  console.log('[firebase-messaging-sw.js] Received background message: ', payload);
  return self.registration.showNotification(payload);
});