const express = require('express');
const httpProxy = require('http-proxy');
const cors = require('cors');

const proxy = httpProxy.createProxyServer({});
const port = 3000;

const app = express();

app.use(cors({
  origin: 'https://fire-base-sensor.web.app', // Change this to your specific origin or origins
  credentials: true,
}));

app.use((req, res) => {
  // Forward the request to the target IP address and port
  proxy.web(req, res, { target: 'http://192.168.57.50' });
});

app.listen(port, () => {
  console.log(`Express server listening on port ${port}`);
});